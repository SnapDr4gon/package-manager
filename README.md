# Mi primera aplicacion en NPM

Aplicacion con node.js utilizando dependencias de manejador de logs, herramienta de refresco en caliente, pruebas
unitarias y lint.

## Índice

- [Requisitos](#requisitos)
- [Instalación](#instalación)
- [USO](#uso)
- [LICENCIA](#licencia)

## Requisitos

- Node.js v14.0 o superior
- NPM v6.0 o superior
  -log4js
  -mocha
  -nodemon
  -Linter

## Instalación

```bash
git clone https://gitlab.com/clase4493809/mi-primera-aplicacion-con-npm
cd mi-primera-aplicacion-con-npm
npm init -y
npm install log4js nodemon mocha eslint --save-dev
```

## Uso

Para iniciar la aplicacion ejecute

```bash
npm start
```

Para ejecutar los test ejecute

```bash
npm test
```

## Licencia

Jared Flores Martinez
353391
Universidad Autonoma de Chihuahua
Web Platforms
