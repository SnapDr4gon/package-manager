const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = "info";

let Hola_lint = 0;

logger.debug("Iniciando aplicacion en modo de pruebas");
logger.info("La app ha iniciado correctamente 2");
logger.warn("Falta el archivo config de la app");
logger.error("No se pude acceder al sistema de archivos del equipo");
logger.fatal("Aplicacion no se pude ejecutar en el so");

function sumar(x, y) {
    return x + y;
}

module.exports = sumar;