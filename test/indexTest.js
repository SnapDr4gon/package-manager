const sumar = require('../index');
const assert = require('assert');

describe("probar la suma de dos numeros", () => {
    //afirmar que es 5
    it("cinco mas cinco es 10", () => {
        assert.equal(10, sumar(5, 5));
    });

    //afirmar que 5 + 5 no son 55
    it("cinco mas 5 no es 55", () => {
        assert.notEqual(55, sumar(5, 5))
    });
});